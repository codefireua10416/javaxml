/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.com.codefire.exchange;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import ua.com.codefire.exchange.models.Exchangerate;
import ua.com.codefire.exchange.models.Exchangerates;
import ua.com.codefire.exchange.models.Row;

/**
 *
 * @author CodeFire
 */
public class Main {

    public static void main(String[] args) throws JAXBException, FileNotFoundException {
        List<Row> list = new ArrayList<>();
        list.add(new Row(new Exchangerate("EUR", "UA", 27.20000, 27.62000)));
        list.add(new Row(new Exchangerate("USD", "UA", 27.20000, 27.62000)));
        list.add(new Row(new Exchangerate("RUR", "UA", 27.20000, 27.62000)));
        Exchangerates exchangerates = new Exchangerates();
        exchangerates.setRows(list);
        
        JAXBContext context = JAXBContext.newInstance(Exchangerates.class);
        Marshaller marshaller = context.createMarshaller();
        marshaller.marshal(exchangerates, new FileOutputStream("test.xml"));
    }
}

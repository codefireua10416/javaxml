/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.com.codefire.exchange.models;

import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author CodeFire
 */
@XmlRootElement
public class Row {

    private Exchangerate exchangerate;

    public Row() {
    }

    public Row(Exchangerate exchangerate) {
        this.exchangerate = exchangerate;
    }

    public Exchangerate getExchangerate() {
        return exchangerate;
    }

    public void setExchangerate(Exchangerate exchangerate) {
        this.exchangerate = exchangerate;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.com.codefire.exchange.api;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import ua.com.codefire.exchange.models.Exchangerates;

/**
 *
 * @author CodeFire
 */
public class ExchangerImpl implements Exchanger {

    private final String path;

    public ExchangerImpl(String path) {
        this.path = path;
    }

    @Override
    public Exchangerates getExchangerates() {
        try {
            URL url = new URL(path);
            JAXBContext context = JAXBContext.newInstance(Exchangerates.class);
            Unmarshaller unmarshaller = context.createUnmarshaller();
            return (Exchangerates) unmarshaller.unmarshal(url);
        } catch (MalformedURLException | JAXBException ex) {
            Logger.getLogger(ExchangerImpl.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

}

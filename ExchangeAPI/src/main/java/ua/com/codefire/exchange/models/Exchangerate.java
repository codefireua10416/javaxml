/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.com.codefire.exchange.models;

import java.io.Serializable;
import java.util.Objects;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * JavaBean
 * @author CodeFire
 */
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Exchangerate implements Serializable {

    @XmlAttribute
    private String ccy;
    @XmlAttribute(name = "base_ccy")
    private String baseCcy;
    @XmlAttribute
    private double buy;
    @XmlAttribute
    private double sale;

    public Exchangerate() {
    }

    public Exchangerate(String ccy, String baseCcy, double buy, double sale) {
        this.ccy = ccy;
        this.baseCcy = baseCcy;
        this.buy = buy;
        this.sale = sale;
    }

    public String getCcy() {
        return ccy;
    }

    public void setCcy(String ccy) {
        this.ccy = ccy;
    }

    public String getBaseCcy() {
        return baseCcy;
    }

    public void setBaseCcy(String baseCcy) {
        this.baseCcy = baseCcy;
    }

    public double getBuy() {
        return buy;
    }

    public void setBuy(double buy) {
        this.buy = buy;
    }

    public double getSale() {
        return sale;
    }

    public void setSale(double sale) {
        this.sale = sale;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 19 * hash + Objects.hashCode(this.ccy);
        hash = 19 * hash + Objects.hashCode(this.baseCcy);
        hash = 19 * hash + (int) (Double.doubleToLongBits(this.buy) ^ (Double.doubleToLongBits(this.buy) >>> 32));
        hash = 19 * hash + (int) (Double.doubleToLongBits(this.sale) ^ (Double.doubleToLongBits(this.sale) >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Exchangerate other = (Exchangerate) obj;
        if (Double.doubleToLongBits(this.buy) != Double.doubleToLongBits(other.buy)) {
            return false;
        }
        if (Double.doubleToLongBits(this.sale) != Double.doubleToLongBits(other.sale)) {
            return false;
        }
        if (!Objects.equals(this.ccy, other.ccy)) {
            return false;
        }
        return Objects.equals(this.baseCcy, other.baseCcy);
    }

    @Override
    public String toString() {
        return "Exchangerate{" + "ccy=" + ccy + ", baseCcy=" + baseCcy + ", buy=" + buy + ", sale=" + sale + '}';
    }

}

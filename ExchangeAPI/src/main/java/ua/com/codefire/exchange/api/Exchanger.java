/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.com.codefire.exchange.api;

import ua.com.codefire.exchange.models.Exchangerates;

/**
 *
 * @author CodeFire
 */
public interface Exchanger {
    Exchangerates getExchangerates();
}

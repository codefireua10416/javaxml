/*
 * Copyright (C) 2016 CodeFireUA <edu@codefire.com.ua>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ua.com.codefire.javaxml;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.IOUtils;

/**
 *
 * @author CodeFireUA <edu@codefire.com.ua>
 */
public class MainXML {

    private static final String GET_XML_COURSE = "https://api.privatbank.ua/p24api/pubinfo?exchange&coursid=5";

    public static void main(String[] args) {

        String xml = null;

        try (InputStream inputStream = new URL(GET_XML_COURSE).openStream()) {
            xml = IOUtils.toString(inputStream);
        } catch (IOException ex) {
            Logger.getLogger(MainXML.class.getName()).log(Level.SEVERE, null, ex);
        }

        System.out.println(xml);
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ua.com.codefire.exchange.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.math.RoundingMode;
import ua.com.codefire.exchange.api.Exchanger;
import ua.com.codefire.exchange.api.ExchangerImpl;
import ua.com.codefire.exchange.models.Exchangerate;
import ua.com.codefire.exchange.models.Exchangerates;
import ua.com.codefire.exchange.models.Row;

/**
 *
 * @author CodeFire
 */
public class Main {
    
    public static void main(String[] args) throws IOException {
        String path = "https://api.privatbank.ua/p24api/pubinfo?exchange&coursid=5";
        BufferedReader keyboard = new BufferedReader(new InputStreamReader(System.in));
        System.out.print("Input UAH: ");
        double uah = Double.parseDouble(keyboard.readLine());
        Exchangerates rates = getExchangerates(path);
        for (Row row : rates.getRows()) {
            // skip BTC to USD
            if("BTC".equals(row.getExchangerate().getCcy())){
                continue;
            }
            printRate(row, uah);
        }
    }

    private static Exchangerates getExchangerates(String path) {
        Exchanger exchanger = new ExchangerImpl(path);
        return exchanger.getExchangerates();
    }

    private static void printRate(Row row, double uah) {
        Exchangerate exchangerate = row.getExchangerate();
        double result = scaled(uah / exchangerate.getSale());
        System.out.println(exchangerate.getCcy() + ": " + result);
    }

    private static double scaled(double result) {
        BigDecimal decimal = new BigDecimal(result);
        return decimal.setScale(5, RoundingMode.HALF_UP).doubleValue();
    }
}

/*
 * Copyright (C) 2016 CodeFireUA <edu@codefire.com.ua>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package ua.com.codefire.javaxml;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.TreeMap;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.io.IOUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 *
 * @author CodeFireUA <edu@codefire.com.ua>
 */
public class MainJSON {

    private static final JSONParser jSONParser = new JSONParser();
    private static final String GET_XML_COURSE = "https://api.privatbank.ua/p24api/pubinfo?json&exchange&coursid=5";

    public static void main(String[] args) {

        String data = null;

        try (InputStream inputStream = new URL(GET_XML_COURSE).openStream()) {
            data = IOUtils.toString(inputStream);
        } catch (IOException ex) {
            Logger.getLogger(MainJSON.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        StringBuilder sb = new StringBuilder();

        try {
            JSONArray array = (JSONArray) jSONParser.parse(data);

            System.out.println("+----------------------------------+");
            System.out.println("| Currency |       BUY |      SALE |");
            System.out.println("+----------------------------------+");

            for (Object obj : array) {
                JSONObject currency = (JSONObject) obj;
                System.out.printf("| %-8s | %9s | %9s |\n", currency.get("ccy"), currency.get("buy"), currency.get("sale"));
                System.out.println("+----------------------------------+");
            }
            
            JSONObject first = (JSONObject) array.get(0);
            
            for (Object key : first.keySet()) {
                sb.append(key).append(",");
            }
            
            sb.reverse().delete(0, 1).reverse().append("\n");
            
            for (Object obj : array) {
                JSONObject entry = (JSONObject)obj;
                TreeMap<Object, Object> treeMap = new TreeMap<>(entry);
                for (Object key : treeMap.keySet()) {
                    Object value = entry.get(key);
                    sb.append(value).append(",");
                }
                sb.reverse().delete(0, 1).reverse().append("\n");
            }
            
            System.out.println(sb);
        } catch (ParseException ex) {
            Logger.getLogger(MainJSON.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
